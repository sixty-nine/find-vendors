<?php

namespace SixtyNine\Utils;

class FindVendors
{
    public static function findVendors($from)
    {
        $curDir = $from;
        while (true) {
            if (is_dir($curDir . '/vendor')) {
                return $curDir . '/vendor';
            }

            if ($curDir === '/') {
                return false;
            }

            $curDir = realpath($curDir . '/..');
        }

        return false;
    }
}
