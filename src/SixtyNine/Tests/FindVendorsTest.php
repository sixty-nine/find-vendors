<?php

namespace SixtyNine\Utils\Tests;

use PHPUnit\Framework\TestCase;
use SixtyNine\Utils\FindVendors;

class FindVendorsTest extends TestCase
{
    public function testFindVendors()
    {
        $this->assertEquals(
            realpath(__DIR__.'/../../../vendor'),
            FindVendors::findVendors(__DIR__)
        );

        $this->assertFalse(FindVendors::findVendors('/foo/bar'));

        $this->assertFalse(FindVendors::findVendors(
            FindVendors::findVendors(FindVendors::findVendors(__DIR__) . '/../../')
        ));
    }
}
