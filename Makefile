test:
	vendor/bin/phpunit -c tests

cs:
	vendor/bin/phpcs --standard=PSR2 --ignore=*/Tests/* src/

cs-fix:
	vendor/bin/phpcbf --standard=PSR2 src/

